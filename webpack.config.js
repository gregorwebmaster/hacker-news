let webpack = require("webpack");
let ExtractTextPlugin = require("extract-text-webpack-plugin");
let LiveReloadPlugin = require("webpack-livereload-plugin");
let path = require('path');

let production = process.env.NODE_ENV === "production" ? true : false;

production ? console.log("Enable production mode.") : null;

    let templateModules = {
        entry: {
            main: [
                './assets/js/main.js',
                './assets/scss/main.scss'
            ]
        },

        output: {
            path: __dirname + '/public/assets/',
            filename: '[name].js'
        },

        module: {
            rules: [{
                test: /\.css$/,
                use: ["style-loader", "css-loader"]
            },

            {
                test: /\.s[ac]ss$/,
                use: ExtractTextPlugin.extract({
                    use: [
                        {
                            loader: "css-loader",
                            options: {
                                sourceMap: true
                            }
                        },
                        {
                            loader: 'postcss-loader',
                            options: {
                                sourceMap: true,
                                plugins: function () {
                                    return [
                                        require('precss'),
                                        require('autoprefixer')
                                    ];
                                }
                            }
                        },
                        {
                            loader: "sass-loader",
                            options: {
                                sourceMap: true,
                                includePaths: [
                                    path.resolve(__dirname, "./node_modules/foundation-sites"),
                                    path.resolve(__dirname, "./node_modules/@fortawesome/fontawesome-free-webfonts"),
                                    path.resolve(__dirname + "/assets/images")
                                ]
                            }
                        }
                    ],
                    fallback: "style-loader"
                })
            },

            {
                test: /\.png|jpe?g|gif$/,
                loaders: [{
                    loader: "file-loader",
                    options: {
                        name: "images/[name].[ext]"
                    }
                },

                    "img-loader"
                ]
            },

            {
                test: /\.svg$/,
                loader: "file-loader",
                options: {
                    name: "images/[name].[ext]"
                },
                include: [
                    path.resolve(__dirname + "./assets/images")
                ]
            },

            {
                test: /\.eot|ttf|woff|woff2|svg$/,
                loader: "file-loader",
                options: {
                    name: "fonts/[name].[ext]"
                },
                include: [
                    path.resolve(__dirname + "/assets/fonts"),
                    path.resolve(__dirname + "/node_modules/@fortawesome/fontawesome-free-webfonts")
                ]
            },

            {
                test: /\.js/,
                loader: "babel-loader",
                exclude: /node_modues/
            }
            ]
        },

        plugins: [
            new ExtractTextPlugin("[name].css"),
            new webpack.ProvidePlugin({
                $: "jquery",
                jQuery: "jquery"
            })
        ]
    };

    if (production) {
        templateModules.plugins.push(
            new webpack.optimize.UglifyJsPlugin({
                beautify: false,
                mangle: {
                    screw_ie8: true,
                    keep_fnames: true
                },
                compress: {
                    screw_ie8: true
                },
                comments: false
            })
        );
    }
    else {
        templateModules.plugins.push(
            new LiveReloadPlugin({
                protocol: "http",
                appendScriptTag: true
            })
        );
    }

    module.exports = templateModules;
