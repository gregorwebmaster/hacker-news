# hacker-news by gregorwebmaster

Training project based on symfony components.

## Goals
1. prepare php applications displaying messages coming from the hacker news API
2. the application should be based on symfony components
3. for frontend should be used Bootstrap
4. it should be a "zero installation" application, it means that the whole process must be carried out in dockers' containers (build, test and run servers)

## running commands
* ```make install``` - install all dependency (composer and NPM)
* ```make composer_install|composer_update``` - install|update composer dependency
* ```make npm_install|npm_update``` - install|update npm dependency
* ```make interactive``` - run builder container in interactive mode (console)
* ```make assets_dev``` - run webpack & build assets
* ```make assets_watch``` - run webpack watch for changes 
* ```make assets_production``` - run webpack and build production assets 
* ```make startup``` - run application server (php & nginx containers) 
* ```make down``` - clean application containers


## Run application localy
* pull this repo
* ```make install```
* ```make assets_dev```
* ```make startup```
* open in browser http://172.11.3.2
* have fun :)