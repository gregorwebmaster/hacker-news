<?php
// define directory
define('BASE_DIR', __DIR__ . '/../');
define('CONFIG_DIR', BASE_DIR . 'config/');
define('CACHE_DIR', BASE_DIR . 'cache/');

//twig configuration
define('TWIG_CONF', [
    'templates_dir' => BASE_DIR . 'templates/',
    'templates_ext' => '.twig',
    'cache' => [
        'cache' => CACHE_DIR,
        'auto_reload' => true
    ]
]);

// HackerNews API configuration
define('HN_API', [
    'url' => 'https://hacker-news.firebaseio.com/v0/',
    'endpoints' => [
        'news' => 'newstories.json',
        'top' => 'topstories.json',
        'best' => 'beststories.json',
        'ask' => 'askstories.json',
        'show' => 'showstories.json',
        'job' => 'jobstories.json',
        'story' => 'item/:id.json',
        'user' => 'user/:id.json'
    ]
]);
