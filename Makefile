BUILDER=gregorwebmaster/dockertools:builder
BUILD_CONTAINER=builder

default: startup
install:
	composer_install
	npm_install

##########################
# Running tasks 		 #
##########################
interactive:
	docker run -v $PWD:/workspace -it --rm --name $(BUILD_CONTAINER) $(BUILDER)  sh

startup:
	docker-compose -f $(PWD)/docker/dev-server.yml up

down:
	docker-compose -f $(PWD)/docker/dev-server.yml down

##########################
# Instalation tasks 	 #
##########################
composer_install:
	docker run -v $(PWD):/workspace -it --rm --name $(BUILD_CONTAINER) $(BUILDER)  composer install

composer_update:
	docker run -v $(PWD):/workspace -it --rm --name $(BUILD_CONTAINER) $(BUILDER)  composer update

npm_install:
	docker run -v $(PWD):/workspace -it --rm --name $(BUILD_CONTAINER) $(BUILDER)  npm install

npm_update:
	docker run -v $(PWD):/workspace -it --rm --name $(BUILD_CONTAINER) $(BUILDER)  npm update


##########################
# Assets tasks		 	 #
##########################
assets_dev:
	docker run -v $(PWD):/workspace -it --rm --name $(BUILD_CONTAINER) $(BUILDER)  npm run dev

assets_watch:
	docker run -v $(PWD):/workspace -it --rm --name $(BUILD_CONTAINER) $(BUILDER)  npm run watch

assets_production:
	docker run -v $(PWD):/workspace -it --rm --name $(BUILD_CONTAINER) $(BUILDER)  npm run production