<?php
/**
 * This file contain controler for stories list.
 * 
 * @author Grzegorz Jaworski <git@gregorwebmaster.com>
 * @license MIT
 * @version 1.0.0
 */
namespace APP\Controllers;

use APP\Providers\TwigProvider;
use APP\Providers\HnApiProvider;
use Carbon\Carbon;

/**
 * Controler of Story List
 * 
 * A Controller to handle the request of story list of following types 
 * news|top|best|ask|show
 */
class StoryListController
{
    /**
     * @var TwigProvider object
     */
    private $view;

    /**
     * @var HnApiProvider object
     */
    private $hnApi;


    public function __construct()
    {
        $this->view = new TwigProvider;
        $this->hnApi = new HnApiProvider;
    }

    /**
     * The main controller method. 
     * As parameters, it receives the data from the request
     * and prepares the response configuration as array(content, code, headers)
     *
     * @param string $type
     * @param integer $page
     * 
     * @return array
     * 
     * @throws Exception
     */
    public function indexAction(string $type, int $page = 1)
    {
        $offset = ($page - 1) * 15;
        try {
            $response = $this->hnApi->getList($type, $offset, 15);
            $response = $this->prepareFinalyData($response);

            return [
                $this->view->renderTemplate('storyList', ['stories' => $response]),
                200,
                ['content-type' => 'text/html']
            ];
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * A method for processing data objects 
     * obtained from the API for use it in a template
     *
     * @param array $data
     * 
     * @return array
     */
    private function prepareFinalyData(array $data): array
    {
        return array_map(
            function ($story) {
                if (!is_array($story)) return;
                
                $story['comments'] = array_key_exists('kids', $story) ? count($story['kids']) : 0;
                $story['elapsedTime'] = array_key_exists('time', $story) ? $this->getElapsedTime($story['time']) : 0;
                return $story;
            },
            $data
        );
    }

    /**
     * A method that prepares information about the time 
     * that has passed since the publication of the story 
     * in a human-understandable format
     *
     * @param integer $datatime
     * 
     * @return string
     */
    private function getElapsedTime(int $datatime): string
    {
        $createDate = Carbon::createFromTimestamp($datatime);
        return $createDate->diffForHumans();
    }
}
