<?php
/**
 * This file contain application bootstrap class.
 * 
 * @author Grzegorz Jaworski <git@gregorwebmaster.com>
 * @license MIT
 * @version 1.0.0
 */
namespace APP;

use Symfony\Component\HttpFoundation\Response;
use APP\Providers\RoutingProvider;


/**
 * Application class
 * 
 * This class create application object.
 */
class App
{
    /**
     * @var RoutingProvider object
     */
    private $router;

    public function __construct()
    {
        $this->router = new RoutingProvider(CONFIG_DIR . 'routing.yml');
    }

    /**
     * Prepare Response object
     * 
     * This method create and return Response
     * For mached request create Response with parameters returned from controller,
     * if request cann't be mach with routing configuration returned respons with status 404.
     * On other exceptions give status 500 with message about current error
     *
     * @return Response object
     */
    public function makeResponse(): Response
    {
        try {
            $controller = $this->router->getController();
            $arguments = $this->router->getArguments($controller);
            
            return new Response(...call_user_func_array($controller, $arguments));
        } catch (ResourceNotFoundException $e) {
            return new Response('Not Found', 404);
        } catch (\Exception $e) {
            return new Response('An error occurred: ' . $e->getMessage() . '<br> File: ' . $e->getFile() . '<br> Line: ' . $e->getLine(), 500);
        }
    }
}
