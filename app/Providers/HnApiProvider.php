<?php
/**
 * This file contain providers of Hacker news API.
 * 
 * @author Grzegorz Jaworski <git@gregorwebmaster.com>
 * @license MIT
 * @version 1.0.0
 */
namespace APP\Providers;

use GuzzleHttp\Client as ApiClient;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Exception\RequestException;

/**
 * Hacker news API provider
 */
class HnApiProvider
{
    /**
     * @var GuzzleHttp\Client object
     */
    private $apiClient;

    public function __construct()
    {
        $this->apiClient = new ApiClient(['base_uri' => HN_API['url']]);
    }

    /**
     * Method that get the list of stories to display on the page
     *
     * @param string $type
     * @param integer $offset
     * @param integer $count
     * 
     * @return array list of stories for current page
     * 
     * @throws RequestException
     */
    public function getList(string $type, int $offset, int $count)
    {
        try {
            $ids = $this->makeRequest('GET', [$type]);
            $ids = $this->getListIds($ids, $offset, $count);

            return $this->getDataAsync(
                $this->getParmsArray(':id', $ids)
            );
        } catch (RequestException $e) {
            return $e;
        }
    }

    /**
     * This method performs API request
     *
     * @param string $method
     * @param array $parms
     * @return array Data from Hacker News API
     * 
     * @throws RequestException
     */
    private function makeRequest(string $method, array $parms)
    {
        try {
            $response = $this->apiClient->request(
                $method,
                $this->getEndpoint(...$parms)
            );

            return json_decode($response->getBody(), true);
        } catch (RequestException $e) {
            return $e;
        }
    }

    /**
     * The method performs asynchronously multiple requests to the API
     *
     * @param array $parms
     * @return array Array of data of successfully request
     */
    private function getDataAsync(array $parms): array
    {
        $result = [];

        $pool = new Pool($this->apiClient, $this->makeMultipleRequest($parms), [
            'concurrency' => 20,
            'fulfilled' => function (Response $response, $index) use (&$result) {
                array_push($result, json_decode($response->getBody(), true));
            },
            'rejected' => function (RequestException $reason, $index) {
                return;
            },
        ]);
        $promise = $pool->promise();
        $promise->wait();
        return $result;
    }

    /**
     * The method preparing collections of requests
     *
     * @param [type] $parms
     */
    private function makeMultipleRequest($parms)
    {
        for ($i = 0; $i < count($parms); $i++) {
            yield new Request('GET', $this->getEndpoint(...$parms[$i]));
        };
    }

    /**
     * Method for preparing the request URL
     *
     * @param string $dataType
     * @param array|null $parms
     * @return string
     */
    private function getEndpoint(string $dataType, ? array $parms = null): string
    {
        $endpoint = HN_API['url'] . HN_API['endpoints'][$dataType];
        if ($parms) {
            $endpoint = strtr($endpoint, $parms);
        }
        return $endpoint;
    }

    /**
     * Method that preparing the list of stories ids to display on the page
     *
     * @param [type] $ids
     * @param [type] $offset
     * @param [type] $count
     * @return array
     */
    private function getListIds($ids, $offset, $count): array
    {
        if ($offset > count($ids)) {
            throw new \Exception('Page not Found');
        }
        return $ids = array_slice($ids, $offset, $count);
    }

    /**
     * The method prepares collections of URL parameters 
     * to prepare the list of endpoints
     *
     * @param string $type
     * @param array $data
     * @return void
     */
    private function getParmsArray(string $type, array $data):array
    {
        return array_map(function ($id) use ($type) {
            return ['story', [$type => $id]];
        }, $data);
    }
}
