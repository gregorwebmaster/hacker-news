<?php
/**
 * This file contain providers of Twig template engine.
 * 
 * @author Grzegorz Jaworski <git@gregorwebmaster.com>
 * @license MIT
 * @version 1.0.0
 */
namespace APP\Providers;

use Twig_Loader_Filesystem;
use Twig_Environment;

/**
 * Twig Templates provider
 */
class TwigProvider
{
    /**
     * @var Twig_Environment
     */
    public $twig;

    public function __construct()
    {
        $this->twig = new Twig_Environment(
            new Twig_Loader_Filesystem(TWIG_CONF['templates_dir']),
            TWIG_CONF['cache']
        );
    }

    /**
     * Method generating HTML 
     * based on the provided data and TWIG template 
     *
     * @param string $template
     * @param array $parms
     * @return string 
     */
    public function renderTemplate(string $template, array $parms) :string
    {
        return $this->twig->render(
            $template . TWIG_CONF['templates_ext'],
            $parms
        );
    }
}
