<?php
/**
 * This file contain providers of Hacker news API.
 * 
 * @author Grzegorz Jaworski <git@gregorwebmaster.com>
 * @license MIT
 * @version 1.0.0
 */
namespace APP\Providers;

use Symfony\Component\Routing\RequestContext;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;

/**
 * Provides Application Routing Object based on Symfony components
 */
class RoutingProvider
{
    private $request;
    private $context;
    private $routesCollection;
    private $urlMatcher;

    public function __construct(string $config)
    {
        $this->request = Request::createFromGlobals();
        $this->context = $this->getContext();
        $this->routesCollection = $this->createRoutesCollection($config);
        $this->urlMatcher = new UrlMatcher($this->routesCollection, $this->context);
        $this->request->attributes->add($this->urlMatcher->match($this->request->getPathInfo()));
    }

    /**
     * The method creates RequestContext object for the current request
     *
     * @return RequestContext
     */
    private function getContext(): RequestContext
    {
        $requestContext = new RequestContext();
        $requestContext->fromRequest($this->request);
        return $requestContext;
    }

    /**
     * The method creates RouteCollection object 
     * based on provided routing configuration
     *
     * @param string $config
     * @return RouteCollection
     */
    private function createRoutesCollection(string $config): RouteCollection
    {
        $routes = Yaml::parseFile($config);
        $colection = new RouteCollection;
        foreach ($routes as $name=>$options) {
            $colection->add($name, new Route(...$this->getRoutingOptions($options)));
        }
        return $colection;
    }

    /**
     * Prepare array with routing opotions for RoutingCollection
     *
     * @param array $options
     * @return array
     */
    private function getRoutingOptions(array $options): array
    {
        $result = [];
        array_walk($options, function ($value) use (&$result) {
            array_push($result, $value);
        });
        return $result;
    }

    /**
     * Prepare controller object for current request
     *
     * @return callable
     */
    public function getController(): callable
    {
        $resolver = new ControllerResolver;
        return $resolver->getController($this->request);
    }

    /**
     * The methods get the GET parameters from the request
     *
     * @param object $controller
     * @return array
     */
    public function getArguments($controller): array
    {
        $resolver = new ArgumentResolver;
        return $resolver->getArguments($this->request, $controller);
    }
}
