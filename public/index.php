<?php
// load app config
require_once '../config/app_conf.php';

// require autoloader
require BASE_DIR . 'vendor/autoload.php';

// start application
$app = new \APP\App;
$response = $app->makeResponse();
$response->send();